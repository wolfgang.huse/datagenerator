#!/usr/local/bin/python3
#
import ssl

import paho.mqtt.client as mqttClient
import json
import time
import base64
import sys

from random import random



# MQTT Parameters
Connected = False
broker_address= str(sys.argv[1]) # Should be the Nutanix edge IP address
port = 1883
topic = str(sys.argv[2])

# Variables that control properties of the generated data.
xRange = [0, 10]  # The range of values taken by the x-coordinate
yRange = [0, 10]  # The range of values taken by the y-coordinate
hotspotSideLength = 1  # The side length of the hotspot
hotspotWeight = 0.2  # The fraction ofpoints that are draw from the hotspots


def generate_point_in_rectangle(x_min, width, y_min, height):
    """Generate points uniformly in the given rectangle."""
    return {
        'x': x_min + random() * width,
        'y': y_min + random() * height
    }

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print("Connected to broker")
        global Connected                #Use global variable
        Connected = True                #Signal connection
    else:
        print("Connection failed")

#def on_publish(client, userdata, result):
	#print ("Message: ", str(message.payload.decode("utf-8")))


def on_message(client, userdata, message):
    #print ("New message received!")
    #print ( "Topic: ", message.topic)
    print ("Topic: ", message.topic , " Message: ", str(message.payload.decode("utf-8")))


class RecordGenerator(object):
    """A class used to generate points used as input to the hotspot detection algorithm. With probability hotspotWeight,
    a point is drawn from a hotspot, otherwise it is drawn from the base distribution. The location of the hotspot
    changes after every 1000 points generated."""

    def __init__(self):
        self.x_min = xRange[0]
        self.width = xRange[1] - xRange[0]
        self.y_min = yRange[0]
        self.height = yRange[1] - yRange[0]
        self.points_generated = 0
        self.hotspot_x_min = None
        self.hotspot_y_min = None

    def get_record(self):
        if self.points_generated % 1000 == 0:
            self.update_hotspot()

        if random() < hotspotWeight:
            record = generate_point_in_rectangle(self.hotspot_x_min, hotspotSideLength, self.hotspot_y_min,
                                                 hotspotSideLength)
            record['is_hot'] = 'Y'
        else:
            record = generate_point_in_rectangle(self.x_min, self.width, self.y_min, self.height)
            record['is_hot'] = 'N'

        self.points_generated += 1
        data = json.dumps(record)
        return {'Data': data}

    def get_records(self, n):
        return [self.get_record() for _ in range(n)]

    def update_hotspot(self):
        self.hotspot_x_min = self.x_min + random() * (self.width - hotspotSideLength)
        self.hotspot_y_min = self.y_min + random() * (self.height - hotspotSideLength)


def main():
    client = mqttClient.Client()
    # Set callbacks for connection event, publish event and message receive event
    client.on_connect = on_connect
    #client.on_publish = on_publish
    client.on_message = on_message
    client.tls_set(ca_certs=str(sys.argv[3]), certfile=str(sys.argv[4]),
                   keyfile=str(sys.argv[5]), cert_reqs=ssl.CERT_REQUIRED,
                   tls_version=ssl.PROTOCOL_TLSv1_2, ciphers=None)
    # Set this to ignore hostname only. TLS is still valid with this setting.
    client.tls_insecure_set(True)
    client.connect(broker_address, port=port)
    #client.subscribe(topic)
    #client.subscribe("display1")
    client.loop_start()

    while Connected != True:  # Wait for connection
        print ("Connecting...")
        time.sleep(1)

    generator = RecordGenerator()
    batch_size = 1

    while True:
        records = generator.get_records(batch_size)
        #print(records)

        output_json=json.dumps(records)

        payload = '{"deviceId": "%s","airt": %d,"unit": "F"}' % (str(sys.argv[6]),(60+random()*15))
        #client.publish(topic, output_json)
        client.publish(topic, payload)

        time.sleep(1)


if __name__ == "__main__":
    main()
